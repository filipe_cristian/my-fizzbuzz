<?php



class FizzBuzzTest extends PHPUnit\Framework\TestCase
{
    public function testFizzBuzz(){
        $fizzBuzz = new App\FizzBuzz();
        $linha = '1,2,Fizz,4,Buzz,Fizz,7,8,Fizz,Buzz,11,Fizz,13,14,FizzBuzz,16,17';
        $this->assertEquals($linha, $fizzBuzz->fizzBuzz(17));
    }
}