<?php /** @noinspection ALL */

namespace App;

class FizzBuzz
{
    /*
     * O programa deve receber um número por parâmetro e escrever os números de 1 até 17
     * Se o número for divisível por 3, no lugar do número escreva Fizz
     * Se o número for divisível por 5, no lugar do número escreva Buzz
     * Se o número for divisível por 3 e 5, no lugar do número escreva FizzBuzz
     * 1,2,Fizz,4,Buzz,Fizz,7,8,Fizz,Buzz,11,Fizz,13,14,FizzBuzz,16,17
     */
    public function fizzBuzz($number) {
        $linha = '';
        for ($i = 1; $i <= $number; $i++) {
            if ($i % 3 == 0 && $i % 5 == 0) {
                $linha .= ',FizzBuzz';
            } else if ($i % 3 == 0) {
                $linha .= ',Fizz';
            } else if ($i % 5 == 0) {
                $linha .= ',Buzz';
            } else {
                $linha .= $i == 1 ? '1' : ",$i";
            }
        }
        return $linha;
    }
}